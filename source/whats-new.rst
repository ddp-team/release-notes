.. _ch-whats-new:

What's new in Debian |RELEASE|
========================================================

The `Wiki <https://wiki.debian.org/NewInTrixie>`__ has more information about this
topic.

.. _supported-archs:

Supported architectures
----------------------------------------------

The following are the officially supported architectures for Debian
|RELEASE|:

-  32-bit PC (``i386``) and 64-bit PC (``amd64``)

-  64-bit ARM (``arm64``)

-  ARM EABI (``armel``)

-  ARMv7 (EABI hard-float ABI, ``armhf``)

-  little-endian MIPS (``mipsel``)

-  64-bit little-endian MIPS (``mips64el``)

-  64-bit little-endian PowerPC (``ppc64el``)

-  IBM System z (``s390x``)

You can read more about port status, and port-specific information for
your architecture at the `Debian port web pages <https://www.debian.org/ports/>`__.

.. _archive-areas:

Archive areas
--------------------------

The following archive areas, mentioned in the Social Contract and in the
Debian Policy, have been around for a long time:

-  main: the Debian distribution;

-  contrib: supplemental packages intended to work with the Debian
   distribution, but which require software outside of the distribution
   to either build or function;

-  non-free: supplemental packages intended to work with the Debian
   distribution that do not comply with the DFSG or have other problems
   that make their distribution problematic.

Following the `2022 General Resolution about non-free
firmware <https://www.debian.org/vote/2022/vote_003>`__, the 5th point
of the Social Contract was extended with the following sentence:

   The Debian official media may include firmware that is otherwise not
   part of the Debian system to enable use of Debian with hardware that
   requires such firmware.

While it's not mentioned explicitly in either the Social Contract or
Debian Policy yet, a new archive area was introduced, making it possible
to separate non-free firmware from the other non-free packages:

-  non-free-firmware

Most non-free firmware packages have been moved from ``non-free`` to
``non-free-firmware`` in preparation for the Debian |RELEASE| release.
This clean separation makes it possible to build official installation
images with packages from ``main`` and from ``non-free-firmware``,
without ``contrib`` or ``non-free``. In turn, these installation images
make it possible to install systems with only ``main`` and
``non-free-firmware``, without ``contrib`` or ``non-free``.

See :ref:`non-free-firmware` for upgrades from |OLDRELEASENAME|.

.. _new-in-distro:

What's new in the distribution?
--------------------------------------------------------------

..
   INFO: Make sure to update the package numbers in the source/conf.py file

This new release of Debian again comes with a lot more software than its
predecessor |OLDRELEASENAME|; the distribution includes over |PACKAGES-NEW|
new packages, for a total of over |PACKAGES-TOTAL| packages. Most of the
software in the distribution has been updated: over |PACKAGES-UPDATED|
software packages (this is |PACKAGES-UPDATE-PERCENT| of all packages in
|OLDRELEASENAME|). Also, a significant number of packages (over
|PACKAGES-REMOVED|, |PACKAGES-REMOVED-PERCENT| of the packages in
|OLDRELEASENAME|) have for various reasons been removed from the
distribution. You will not see any updates for these packages and they
will be marked as "obsolete" in package management front-ends; see
:ref:`obsolete`.

.. _major-packages:

Desktops and well known packages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Debian again ships with several desktop applications and environments.
Among others it now includes the desktop environments GNOME 43, KDE
Plasma 5.27, LXDE 11, LXQt 1.2.0, MATE 1.26, and Xfce 4.18.

Productivity applications have also been upgraded, including the office
suites:

-  LibreOffice is upgraded to version 7.4;

-  GNUcash is upgraded to 4.13;

Among many others, this release also includes the following software
updates:

+----------------------+----------------------+----------------------+
| Package              | Version in           | Version in |RELEASE| |
|                      | |OLDRELEASE|         | (|RELEASENAME|)      |
|                      | (|OLDRELEASENAME|)   |                      |
+======================+======================+======================+
| Apache               | 2.4.54               | 2.4.57               |
+----------------------+----------------------+----------------------+
| Bash                 | 5.1                  | 5.2.15               |
+----------------------+----------------------+----------------------+
| BIND DNS Server      | 9.16                 | 9.18                 |
+----------------------+----------------------+----------------------+
| Cryptsetup           | 2.3                  | 2.6                  |
+----------------------+----------------------+----------------------+
| Emacs                | 27.1                 | 28.2                 |
+----------------------+----------------------+----------------------+
| Exim default         | 4.94                 | 4.96                 |
| e-mail server        |                      |                      |
+----------------------+----------------------+----------------------+
| GNU Compiler         | 10.2                 | 12.2                 |
| Collection as        |                      |                      |
| default compiler     |                      |                      |
+----------------------+----------------------+----------------------+
| GIMP                 | 2.10.22              | 2.10.34              |
+----------------------+----------------------+----------------------+
| GnuPG                | 2.2.27               | 2.2.40               |
+----------------------+----------------------+----------------------+
| Inkscape             | 1.0.2                | 1.2.2                |
+----------------------+----------------------+----------------------+
| the GNU C library    | 2.31                 | 2.36                 |
+----------------------+----------------------+----------------------+
| Linux kernel image   | 5.10 series          | 6.1 series           |
+----------------------+----------------------+----------------------+
| LLVM/Clang toolchain | 9.0.1 and 11.0.1     | 13.0.1 and 14.0      |
|                      | (default) and 13.0.1 | (default) and 15.0.6 |
+----------------------+----------------------+----------------------+
| MariaDB              | 10.5                 | 10.11                |
+----------------------+----------------------+----------------------+
| Nginx                | 1.18                 | 1.22                 |
+----------------------+----------------------+----------------------+
| OpenJDK              | 11                   | 17                   |
+----------------------+----------------------+----------------------+
| OpenLDAP             | 2.4.57               | 2.5.13               |
+----------------------+----------------------+----------------------+
| OpenSSH              | 8.4p1                | 9.2p1                |
+----------------------+----------------------+----------------------+
| OpenSSL              | 1.1.1n               | 3.0.8                |
+----------------------+----------------------+----------------------+
| Perl                 | 5.32                 | 5.36                 |
+----------------------+----------------------+----------------------+
| PHP                  | 7.4                  | 8.2                  |
+----------------------+----------------------+----------------------+
| Postfix MTA          | 3.5                  | 3.7                  |
+----------------------+----------------------+----------------------+
| PostgreSQL           | 13                   | 15                   |
+----------------------+----------------------+----------------------+
| Python 3             | 3.9.2                | 3.11.2               |
+----------------------+----------------------+----------------------+
| Rustc                | 1.48                 | 1.63                 |
+----------------------+----------------------+----------------------+
| Samba                | 4.13                 | 4.17                 |
+----------------------+----------------------+----------------------+
| Systemd              | 247                  | 252                  |
+----------------------+----------------------+----------------------+
| Vim                  | 8.2                  | 9.0                  |
+----------------------+----------------------+----------------------+

.. _man-xx:

More translated man pages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Thanks to our translators, more documentation in ``man``-page format is
available in more languages than ever. For example, many man pages are
now available in Czech, Danish, Greek, Finnish, Indonesian, Macedonian,
Norwegian (Bokmål), Russian, Serbian, Swedish, Ukrainian and Vietnamese,
and all systemd man pages are now available in German.

To ensure the ``man`` command shows the documentation in your language
(where possible), install the right manpages-*lang* package and make sure
your locale is correctly configured by using

.. code-block:: console

   # dpkg-reconfigure locales

.

.. _news-from-med-blend:

News from Debian Med Blend
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As in every release new packages have been added in the fields of
medicine and life sciences.

The Debian Med team is always interested in feedback from users,
especially in the form of requests for packaging of not-yet-packaged
free software, or for backports from new packages or higher versions in
testing.

To install packages maintained by the Debian Med team, install the
metapackages named ``med-*``, which are at version 3.8.x for Debian
bookworm. Feel free to visit the `Debian Med tasks
pages <https://blends.debian.org/med/tasks>`__ to see the full range of
biological and medical software available in Debian.

.. _news-from-astro-blend:

News from Debian Astro Blend
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Check `the Astro Blend page <https://blends.debian.org/astro>`__ for a
complete list and further information.

.. _dummy-ch:
.. only:: fixme

	Something
	~~~~~~~~~~~~~~~~~~

	Text
