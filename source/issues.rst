.. _ch-information:

Issues to be aware of for |RELEASENAME|
==========================================================================

Sometimes, changes introduced in a new release have side-effects we
cannot reasonably avoid, or they expose bugs somewhere else. This
section documents issues we are aware of. Please also read the errata,
the relevant packages' documentation, bug reports, and other information
mentioned in :ref:`morereading`.

.. _upgrade-specific-issues:

Upgrade specific items for |RELEASENAME|
----------------------------------------------------------------------------

This section covers items related to the upgrade from |OLDRELEASENAME| to
|RELEASENAME|.

.. _openssh-pam-environment-removed:

openssh-server no longer reads ~/.pam_environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Secure Shell (SSH) daemon provided in the **openssh-server** package,
which allows logins from remote systems, no longer reads the user's
``~/.pam_environment`` file by default; this feature has a `history of
security problems <https://bugs.debian.org/1030119>`__ and has been
deprecated in current versions of the Pluggable Authentication Modules (PAM)
library.  If you used this feature, you should switch from setting variables
in ``~/.pam_environment`` to setting them in your shell initialization files
(e.g. ``~/.bash_profile`` or ``~/.bashrc``) or some other similar mechanism
instead.

Existing SSH connections will not be affected, but new connections may
behave differently after the upgrade.  If you are upgrading remotely, it is
normally a good idea to ensure that you have some other way to log into the
system before starting the upgrade; see :ref:`recovery`.


.. _openssh-dsa-removal:

OpenSSH no longer supports DSA keys
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Digital Signature Algorithm (DSA) keys, as specified in the Secure Shell
(SSH) protocol, are inherently weak: they are limited to 160-bit private
keys and the SHA-1 digest.  The SSH implementation provided by the
**openssh-client** and **openssh-server** packages has disabled support for
DSA keys by default since OpenSSH 7.0p1 in 2015, released with Debian 9
("stretch"), although it could still be enabled using the
``HostKeyAlgorithms`` and ``PubkeyAcceptedAlgorithms`` configuration options
for host and user keys respectively.

The only remaining uses of DSA at this point should be connecting to some
very old devices.  For all other purposes, the other key types supported by
OpenSSH (RSA, ECDSA, and Ed25519) are superior.

As of OpenSSH 9.8p1 in trixie, DSA keys are no longer supported even with
the above configuration options.  If you have a device that you can only
connect to using DSA, then you can use the ``ssh1`` command provided by the
**openssh-client-ssh1** package to do so.

In the unlikely event that you are still using DSA keys to connect to a
Debian server (if you are unsure, you can check by adding the ``-v`` option
to the ``ssh`` command line you use to connect to that server and looking
for the "Server accepts key:" line), then you must generate replacement keys
before upgrading.  For example, to generate a new Ed25519 key and enable
logins to a server using it, run this on the client, replacing
``username@server`` with the appropriate user and host names:

.. code-block:: console

   $ ssh-keygen -t ed25519
   $ ssh-copy-id username@server


.. _before-first-reboot:

Things to do post upgrade before rebooting
--------------------------------------------------------------------------------

When ``apt full-upgrade`` has finished, the "formal" upgrade is
complete. For the upgrade to |RELEASENAME|, there are no special actions
needed before performing a reboot.

.. only:: fixme

	When ``apt full-upgrade`` has finished, the "formal" upgrade is
	complete, but there are some other things that should be taken care of
	*before* the next reboot.

	::

	   add list of items here
       
         

.. _not-upgrade-only:

Items not limited to the upgrade process
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _limited-security-support:

Limitations in security support
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are some packages where Debian cannot promise to provide minimal
backports for security issues. These are covered in the following
subsections.

.. note::

   The package **debian-security-support** helps to track the security
   support status of installed packages.

.. _browser-security:

Security status of web browsers and their rendering engines
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Debian |RELEASE| includes several browser engines which are affected by a
steady stream of security vulnerabilities. The high rate of
vulnerabilities and partial lack of upstream support in the form of long
term branches make it very difficult to support these browsers and
engines with backported security fixes. Additionally, library
interdependencies make it extremely difficult to update to newer
upstream releases. Applications using the **webkit2gtk** source package
(e.g. **epiphany**) are covered by security support, but applications using
qtwebkit (source package **qtwebkit-opensource-src**) are not.

For general web browser use we recommend Firefox or Chromium. They will
be kept up-to-date by rebuilding the current ESR releases for stable.
The same strategy will be applied for Thunderbird.

Once a release becomes ``oldstable``, officially supported browsers may
not continue to receive updates for the standard period of coverage. For
example, Chromium will only receive 6 months of security support in
``oldstable`` rather than the typical 12 months.

.. _golang-static-linking:

Go- and Rust-based packages
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Debian infrastructure currently has problems with rebuilding
packages of types that systematically use static linking. With the
growth of the Go and Rust ecosystems it means that these packages will
be covered by limited security support until the infrastructure is
improved to deal with them maintainably.

In most cases if updates are warranted for Go or Rust development
libraries, they will only be released via regular point releases.

.. _python3-pep-668:

Python Interpreters marked externally-managed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Debian provided python3 interpreter packages (**python3.11** and **pypy3**)
are now marked as being externally-managed, following
`PEP-668 <https://peps.python.org/pep-0668/>`__. The version of
**python3-pip** provided in Debian follows this, and will refuse to manually
install packages on Debian's python interpreters, unless the
``--break-system-packages`` option is specified.

If you need to install a Python application (or version) that isn't
packaged in Debian, we recommend that you install it with ``pipx`` (in
the **pipx** Debian package). ``pipx`` will set up an environment isolated
from other applications and system Python modules, and install the
application and its dependencies into that.

If you need to install a Python library module (or version) that isn't
packaged in Debian, we recommend installing it into a virtualenv, where
possible. You can create virtualenvs with the ``venv`` Python stdlib
module (in the **python3-venv** Debian package) or the ``virtualenv`` Python
3rd-party tool (in the **virtualenv** Debian package). For example, instead
of running
``pip install --user foo``, run: ``mkdir -p ~/.venvs && python3 -m venv ~/.venvs/foo && ~/.venvs/foo/bin/python -m pip install foo``
to install it in a dedicated virtualenv.

See ``/usr/share/doc/python3.11/README.venv`` for more details.

.. _vlc-ffmpeg-5:

Limited hardware-accelerated video encoding/decoding support in VLC
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The VLC video player supports hardware-accelerated video decoding and
encoding via VA-API and VDPAU. However, VLC's support for VA-API is
tightly related to the version of FFmpeg. Because FFmpeg was upgraded to
the 5.x branch, VLC's VA-API support has been disabled. Users of GPUs
with native VA-API support (e.g., Intel and AMD GPUs) may experience
high CPU usage during video playback and encoding.

Users of GPUs offering native VDPAU support (e.g., NVIDIA with non-free
drivers) are not affected by this issue.

Support for VA-API and VDPAU can be checked with ``vainfo`` and
``vdpauinfo`` (each provided in a Debian package of the same name).

.. _systemd-resolved:

systemd-resolved has been split into a separate package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The new **systemd-resolved** package will not be installed automatically on
upgrades. If you were using the ``systemd-resolved`` system service,
please install the new package manually after the upgrade, and note that
until it has been installed, DNS resolution might no longer work since
the service will not be present on the system. Installing this package
will automatically give systemd-resolved control of
``/etc/resolv.conf``. For more information about systemd-resolved,
consult the official
`documentation <https://www.freedesktop.org/software/systemd/man/systemd-resolved.service.html>`__.
Note that systemd-resolved was not, and still is not, the default DNS
resolver in Debian. If you have not configured your machine to use
systemd-resolved as the DNS resolver, no action is required.

.. _systemd-boot:

systemd-boot has been split into a separate package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The new **systemd-boot** package will not be installed automatically on
upgrades. If you were using ``systemd-boot``, please install this new
package manually, and note that until you do so, the older version of
systemd-boot will be used as the bootloader. Installing this package
will automatically configure systemd-boot as the machine's bootloader.
The default boot loader in Debian is still GRUB. If you have not
configured the machine to use systemd-boot as the bootloader, no action
is required.

.. _systemd-journal-remote:

systemd-journal-remote no longer uses GnuTLS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The optional
`systemd-journal-gatewayd <https://www.freedesktop.org/software/systemd/man/systemd-journal-remote.service.html#--trust=>`__
and
`systemd-journal-remote <https://www.freedesktop.org/software/systemd/man/systemd-journal-gatewayd.service.html#--trust=>`__
services are now built without GnuTLS support, which means the
``--trust`` option is no longer provided by either program, and an error
will be raised if it is specified.

.. _adduser-changes:

Extensive changes in adduser for bookworm
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There have been several changes in **adduser**. The most prominent change is
that ``--disabled-password`` and ``--disabled-login`` are now
functionally identical. For further details, please read the
``/usr/share/doc/adduser/NEWS.Debian.gz``.

.. _xen-network:

Predictable naming for Xen network interfaces
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The predictable naming logic in systemd for network interfaces has been
extended to generate stable names from Xen netfront device information.
This means that instead of the former system of names assigned by the
kernel, interfaces now have stable names of the form ``enX#``. Please
adapt your system before rebooting after the upgrade. Some more
information can be found on the `NetworkInterfaceNames wiki
page <https://wiki.debian.org/NetworkInterfaceNames#bookworm-xen>`__.

.. _dash-circumflex:

Change in dash handling of circumflex
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``dash``, which by default provides the system shell ``/bin/sh`` in
Debian, has switched to treating the circumflex (``^``) as a literal
character, as was always the intended POSIX-compliant behavior. This
means that in bookworm ``[^0-9]`` no longer means "not 0 to 9" but "0 to
9 and ^".

.. _netcat-openbsd-now-supports-abstract-sockets:

netcat-openbsd supports abstract sockets
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The ``netcat`` utility for reading and writing data across network
connections supports abstract sockets
(see :url-man-stable:`unix.7.html#Abstract_sockets`,
and uses them by default in some circumstances.

By default, ``netcat`` is provided by **netcat-traditional**. However, if
``netcat`` is provided by the **netcat-openbsd** package and you are using
an ``AF_UNIX`` socket, then this new default applies. In this case the
``-U`` option to ``nc`` will now interpret an argument starting with an
``@`` as requesting an abstract socket rather than as a filename
beginning with an ``@`` in the current directory. This can have security
implications because filesystem permissions can no longer be used to
control access to an abstract socket. You can continue to use a filename
starting with an ``@`` by prefixing the name with ``./`` or by
specifying an absolute path.

.. _obsolescense-and-deprecation:

Obsolescence and deprecation
--------------------------------------------------------

.. _noteworthy-obsolete-packages:

Noteworthy obsolete packages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following is a list of known and noteworthy obsolete packages (see
:ref:`obsolete` for a description).

The list of obsolete packages includes:

-  The **libnss-ldap** package has been removed from |RELEASENAME|. Its
   functionalities are now covered by **libnss-ldapd** and **libnss-sss**.

-  The **libpam-ldap** package has been removed from |RELEASENAME|. Its
   replacement is **libpam-ldapd**.

-  The **fdflush** package has been removed from |RELEASENAME|. In its stead,
   please use ``blockdev --flushbufs`` from **util-linux**.

-  The **libgdal-perl** package has been removed from |RELEASENAME|, because
   the Perl binding for GDAL is no longer supported upstream. If you
   need Perl support for GDAL, you can migrate to the FFI interface
   provided by the **Geo::GDAL::FFI** package, available on CPAN. You will
   have to build your own binaries as documented on the
   `BookwormGdalPerl Wiki page <https://wiki.debian.org/BookwormGdalPerl>`__.

.. _deprecated-components:

Deprecated components for |RELEASENAME|
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

With the next release of Debian |NEXTRELEASE| (codenamed |NEXTRELEASENAME|)
some features will be deprecated. Users will need to migrate to other
alternatives to prevent trouble when updating to Debian |NEXTRELEASE|.

This includes the following features:

-  Development of the NSS service ``gw_name`` stopped in 2015. The
   associated package **libnss-gw-name** may be removed in future Debian
   releases. The upstream developer suggests using **libnss-myhostname**
   instead.

-  **dmraid** has not seen upstream activity since end 2010 and has been on
   life support in Debian. bookworm will be the last release to ship it,
   so please plan accordingly if you're using **dmraid**.

-  **request-tracker4** has been superseded by **request-tracker5** in this
   release, and will be removed in future releases. We recommend that
   you plan to migrate from **request-tracker4** to **request-tracker5** during
   the lifetime of this release.

-  The **isc-dhcp** suite has been
   `deprecated <https://www.isc.org/blogs/isc-dhcp-eol/>`__ by the
   `ISC <https://www.isc.org/>`__. The `Debian Wiki <https://wiki.debian.org/>`__ has
   a list of alternative implementations, see the `DHCP
   Client <https://wiki.debian.org/DHCP_Client>`__ and `DHCP
   Server <https://wiki.debian.org/DHCP_Server>`__ pages for the latest. If you are
   using NetworkManager or systemd-networkd, you can safely remove the
   **isc-dhcp-client** package as they both ship their own implementation.
   If you are using the **ifupdown** package, you can experiment with udhcpc
   as a replacement. The ISC recommends the **Kea** package as a replacement
   for DHCP servers.

   The security team will support the **isc-dhcp** package during the
   bookworm lifetime, but the package will likely be unsupported in the
   next stable release, see `bug #1035972 (isc-dhcp
   EOL'ed) <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1035972>`__ for more details.

.. only:: fixme

   No-longer-supported hardware
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   For a number of \`arch`-based devices that were supported in
   |OLDRELEASENAME|, it is no longer viable for Debian to build the required
   ``Linux`` kernel, due to hardware limitations. The unsupported devices
   are:

   -  foo

   Users of these platforms who wish to upgrade to |RELEASENAME| nevertheless
   should keep the |OLDRELEASENAME| APT sources enabled. Before upgrading
   they should add an APT preferences file containing:

   .. parsed-literal::

      Package: linux-image-marvell
      Pin: release n= |OLDRELEASENAME|
      Pin-Priority: 900

   The security support for this configuration will only last until
   |OLDRELEASENAME|'s End Of Life.

.. _rc-bugs:

Known severe bugs
---------------------------------------------------

Although Debian releases when it's ready, that unfortunately doesn't
mean there are no known bugs. As part of the release process all the
bugs of severity serious or higher are actively tracked by the Release
Team, so an `overview of those
bugs <https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=release.debian.org@packages.debian.org;tag=&releasename;-can-defer>`__
that were tagged to be ignored in the last part of releasing |RELEASENAME|
can be found in the `Debian Bug Tracking System <https://bugs.debian.org/>`__. The
following bugs were affecting |RELEASENAME| at the time of the release and
worth mentioning in this document:

+----------------------+---------------------------+------------------------------+
| Bug number           | Package (source or        | Description                  |
|                      | binary)                   |                              |
+======================+===========================+==============================+
| `1032240`_           | **akonadi-backend-mysql** | akonadi server fails         |
|                      |                           | to start since it            |
|                      |                           | cannot connect to            |
|                      |                           | mysql database               |
+----------------------+---------------------------+------------------------------+
| `1032177`_           | **faketime**              | faketime doesn't             |
|                      |                           | fake time (on i386)          |
+----------------------+---------------------------+------------------------------+
| `918984`_            | **src:fuse3**             | provide upgrade path         |
|                      |                           | fuse -> fuse3 for            |
|                      |                           | bookworm                     |
+----------------------+---------------------------+------------------------------+
| `1016903`_           | **g++-12**                | tree-vectorize:              |
|                      |                           | Wrong code at O2             |
|                      |                           | level                        |
|                      |                           | (-fno-tree-vectorize         |
|                      |                           | is working)                  |
+----------------------+---------------------------+------------------------------+
| `1020284`_           | **git-daemon-run**        | fails to purge:              |
|                      |                           | deluser -f: Unknown          |
|                      |                           | option: f                    |
+----------------------+---------------------------+------------------------------+
| `919296`_            | **git-daemon-run**        | fails with 'warning:         |
|                      |                           | git-daemon: unable           |
|                      |                           | to open                      |
|                      |                           | supervise/ok: file           |
|                      |                           | does not exist'              |
+----------------------+---------------------------+------------------------------+
| `1034752`_           | **src:gluegen2**          | embeds non-free headers      |
+----------------------+---------------------------+------------------------------+

.. _1032240: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1032240
.. _1032177: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1032177
.. _918984: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=918984
.. _1016903: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1016903
.. _1020284: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1020284
.. _919296: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=919296
.. _1034752: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1034752
