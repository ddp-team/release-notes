.. _ch-installing:

Installation System
======================================

The Debian Installer is the official installation system for Debian. It
offers a variety of installation methods. The methods that are available
to install your system depend on its architecture.

Images of the installer for |RELEASENAME| can be found together with the
Installation Guide on the Debian website (|URL-INSTALLER|).

The Installation Guide is also included on the first media of the
official Debian DVD (CD/blu-ray) sets, at:

.. code-block :: console

   /doc/install/manual/language/index.html

You may also want to check the errata for debian-installer at
|URL-INSTALLER-ERRATA| for a list of known issues.

.. _inst-new:

What's new in the installation system?
----------------------------------------------------------------------------

There has been a lot of development on the Debian Installer since its
previous official release with Debian |OLDRELEASE|, resulting in improved
hardware support and some exciting new features or improvements.

If you are interested in an overview of the changes since
|OLDRELEASENAME|, please check the release announcements for the
|RELEASENAME| beta and RC releases available from the Debian Installer's
`news history <https://www.debian.org/devel/debian-installer/News/>`__.

.. _inst-changes:
.. only:: fixme

	Something
	~~~~~~~~~~~~~~~~~~

	Text

.. _cloud:

Cloud installations
--------------------------------------

The `cloud team <https://wiki.debian.org/Teams/Cloud>`__ publishes Debian |RELEASENAME| for
several popular cloud computing services including:

-  Amazon Web Services

-  Microsoft Azure

-  OpenStack

-  Plain VM

Cloud images provide automation hooks via ``cloud-init`` and prioritize
fast instance startup using specifically optimized kernel packages and
grub configurations. Images supporting different architectures are
provided where appropriate and the cloud team endeavors to support all
features offered by the cloud service.

The cloud team will provide updated images until the end of the LTS
period for |RELEASENAME|. New images are typically released for each point
release and after security fixes for critical packages. The cloud team's
full support policy can be found
`here <https://wiki.debian.org/Cloud/ImageLifecycle>`__.

More details are available at `<https://cloud.debian.org/>`__ and
`on the wiki <https://wiki.debian.org/Cloud/>`__.

.. _containers:

Container and Virtual Machine images
------------------------------------------------------------------------

Multi-architecture Debian |RELEASENAME| container images are available on
`Docker Hub <https://hub.docker.com/_/debian>`__.
In addition to the standard images, a
"slim" variant is available that reduces disk usage.

Virtual machine images for the Hashicorp Vagrant VM manager are
published to `Vagrant Cloud <https://app.vagrantup.com/debian>`__.
