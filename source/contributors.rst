.. _ap-contrib:

Contributors to the Release Notes
==================================================================

Many people helped with the release notes, including, but not limited to

*  :abbr:`Adam D. Barrat (various fixes in 2013)`,
*  :abbr:`Adam Di Carlo (previous releases)`,
*  :abbr:`Andreas Barth aba (previous releases: 2005 - 2007)`,
*  :abbr:`Andrei Popescu (various contributions)`,
*  :abbr:`Anne Bezemer (previous release)`,
*  :abbr:`Bob Hilliard (previous release)`,
*  :abbr:`Charles Plessy (description of GM965 issue)`,
*  :abbr:`Christian Perrier bubulle (Lenny installation)`,
*  :abbr:`Christoph Berg (PostgreSQL-specific issues)`,
*  :abbr:`Daniel Baumann (Debian Live)`,
*  :abbr:`David Prévot taffit (Wheezy release)`,
*  :abbr:`Eddy Petrișor (various contributions)`,
*  :abbr:`Emmanuel Kasper (backports)`,
*  :abbr:`Esko Arajärvi (rework X11 upgrade)`,
*  :abbr:`Frans Pop fjp (previous release Etch)`,
*  :abbr:`Giovanni Rapagnani (innumerable contributions)`,
*  :abbr:`Gordon Farquharson (ARM port issues)`,
*  :abbr:`Hideki Yamane henrich (contributed and contributing since 2006)`,
*  :abbr:`Holger Wansing holgerw (contributed and contributing since 2009)`,
*  :abbr:`Javier Fernández-Sanguino Peña jfs (Etch release, Squeeze release)`,
*  :abbr:`Jens Seidel (German translation, innumerable contributions)`,
*  :abbr:`Jonas Meurer (syslog issues)`,
*  :abbr:`Jonathan Nieder (Squeeze release, Wheezy release)`,
*  :abbr:`Joost van Baal-Ilić joostvb (Wheezy release, Jessie release)`,
*  :abbr:`Josip Rodin (previous releases)`,
*  :abbr:`Julien Cristau jcristau (Squeeze release, Wheezy release)`,
*  :abbr:`Justin B Rye (English fixes)`,
*  :abbr:`LaMont Jones (description of NFS issues)`,
*  :abbr:`Luk Claes (editors motivation manager)`,
*  :abbr:`Martin Michlmayr (ARM port issues)`,
*  :abbr:`Michael Biebl (syslog issues)`,
*  :abbr:`Moritz Mühlenhoff (various contributions)`,
*  :abbr:`Niels Thykier nthykier (Jessie release)`,
*  :abbr:`Noah Meyerhans (innumerable contributions)`,
*  :abbr:`Noritada Kobayashi (Japanese translation (coordination), innumerable contributions)`,
*  :abbr:`Osamu Aoki (various contributions)`,
*  :abbr:`Paul Gevers elbrus (buster release)`,
*  :abbr:`Peter Green (kernel version note)`,
*  :abbr:`Rob Bradford (Etch release)`,
*  :abbr:`Samuel Thibault (description of d-i Braille support)`,
*  :abbr:`Simon Bienlein (description of d-i Braille support)`,
*  :abbr:`Simon Paillard spaillar-guest (innumerable contributions)`,
*  :abbr:`Stefan Fritsch (description of Apache issues)`,
*  :abbr:`Steve Langasek (Etch release)`,
*  :abbr:`Steve McIntyre (Debian CDs)`,
*  :abbr:`Tobias Scherer (description of "proposed-update")`,
*  :abbr:`victory victory-guest (markup fixes, contributed and contributing since 2006)`,
*  :abbr:`Vincent McIntyre (description of "proposed-update")`,
*  :abbr:`W. Martin Borgert (editing Lenny release, switch to DocBook XML)`.

This document has been translated into many languages. Many thanks to all
the translators!
