Release Notes
-------------

The Release Notes contain important information for people updating
from the previous version of Debian, particularly for less experienced users.

The Release Notes are written in reStructuredText, using the sphinx converter,
and are output in various formats, such as HTML, text, and PDF.

You can find (outdated) meta data about the Release Notes at
https://wiki.debian.org/ReleaseNotes
and
https://www.debian.org/doc/user-manuals#relnotes


Building the Release Notes
--------------------------

To build the release notes, you need these packages:
- latexmk
- python3-distro-info
- python3-sphinx
- python3-stemmer
- python3-sphinx-rtd-theme
- tex-gyre
- texinfo
- texlive-fonts-recommended
- texlive-lang-all
- texlive-latex-extra
- texlive-latex-recommended

See also the debian/control file.

Just call "make" to build everything. To build the release notes only in a
single format (html, txt, pdf) for just one language (e.g. zh_CN, en, de),
call e.g.: "make html LANGS=de".

Contributing
------------

To submit new text for the Release Notes you can submit a merge
request against the repository at
https://salsa.debian.org/ddp-team/release-notes or send a patch via
the bts (against the release-notes pseudo-package).

For example, to document an issue in the next Debian release, you
would add a new section to the file 'sources/issues.rst'. It should
look like:

```rst
.. _name-of-issue:

A title describing the issue
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Introduce the issue, and explain briefly what the relevant packages
do. This helps people reading the release notes decide whether they
need to read this section or not. It may also bring the package to
the attention of potential users.

More details about the issue. Make sure to explain things simply,
for non-experts. Users generally want to know what the issue is, how
it affects them, and what they can do about it. This is particularly
important when describing new features that may break existing
system: how can they revert the change if it not suitable for them?

Use as many paragraphs and sentences as needed, but try and keep
things as short as possible.
```

The `name-of-issue` is an abbreviated version of the section title
(a leading `_` and spaces replaced by `-`): this is used to link to the
section. The line of `~` should be a bit longer as the title, to avoid
build errors, if translations are longer than the English original;
and the blank lines are required.

The contents are written in reStructuredText (which is described at
<https://docutils.sourceforge.io/docs/user/rst/quickstart.html>. The
style for the Release Notes is set out at
<http://jbr.me.uk/linux/esl.html> and the following examples explain
how to mark it up:

- To refer to the name of a Debian package:
    `The **apt** package manager is now the default.`;

- To mark text as important:
   `It is very important to do this **before** rebooting.`;

- To reference a man page (using manpages.debian.org):
     ``See :url-man-stable:`apt.1` for more.``
  To reference a man page in the previous release replace
  `url-man-stable` with `url-man-oldstable`;

- To refer to a command that the user will run:
```rst
Run the commands:

.. code-block:: console
   $ apt search apt
   $ ls /home

This text is not part of the code-block.
```

  The blank lines and indentation are required;

- If a command should be run as root, use a `code-block`
  as above but replace the `$` with a `#`: do not suggest the use of
  `sudo` for this because this is not enabled by default in Debian;

- To link to a website with more information:
  ``See the Debian `homepage <https://www.debian.org/>__` for more information.``
    (the part between the ``` and the `<` will be the anchor for the link);

- To refer to specific Debian release names, write the name in lower
  case without markup. This means it is preferable to avoid having a
  release name at the start of a sentence:
  `The trixie release has many more packages than bookworm.`;

  You can also use `|OLDRELEASENAME|` and `|RELEASENAME|` for text
  that is likely to remain in the release notes for several releases
  (this is rare for new content);

- The word `Debian` itself should always have an upper case `D` and no
  special markup;

- For all other concepts requiring markup (commands, filenames,
    fragments from configuration files, variables, output, keystrokes etc) use
    double backticks:
    `The ``apt-get`` command from the **apt** package respects the
    ``LANG`` environment variable and the contents of
    ``/etc/apt/sources.list.d``.`;

- To link to a section in the same file that you are editing:
  ``See `Purging removed packages <#purge-removed-packages>`__ for more information.``.

  The `<#purge-removed-packages>` identifies the section starting with
  `.. _purge-removed-packages:`;

- To link to a section in a different file:
  ``We suggest that before upgrading you also read the :ref:`ch-information` chapter.``
  where ``.. _ch-information:`` identifies the section to link to;

- For quotes, always use `"double quotes"`. Never use `“curved
  quotes”`, `»guillemets«` or single quotes. The double quotes will
  be automatically converted to the most appropriate quote mark for
  each language when the Release Notes are built;

- To make a list of points, use semicolons and a final `.' (or no
  punctuation for very short lists), as recommended by
  <http://jbr.me.uk/linux/esl.html#e5>.

```
The following archive areas, mentioned in the Social Contract and in the
Debian Policy, have been around for a long time:

-  `main`: the Debian distribution;

-  `contrib`: supplemental packages intended to work with the Debian
   distribution, but which require software outside of the distribution
   to either build or function;

-  `non-free`: supplemental packages intended to work with the Debian
   distribution that do not comply with the DFSG or have other problems
   that make their distribution problematic.
```

- To temporarily prevent a section appearing in the output, but keeping it in
  the document (and in the `po` files used by translators), insert
  `..only:: fixme` followed by a blank line before the paragraph.
  The content of the section itself needs to be indented:

```rst
  .. only:: fixme

      No-longer-supported hardware
      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      A a number of [devices - unclear] that were supported in
      |OLDRELEASENAME| are no longer supported in |RELEASENAME| because
        [needs a better explanation of why]

    This includes:

    -  i386 [if actually unsupported?];

    -  etc.
```

Translations
------------

Translators, please refer to the `README.translators` document for
a full description of the translation process for this document.

Autobuilding of the Release Notes
---------------------------------

The Release Notes are automatically built on `www-master.debian.org`
and published at
`https://www.debian.org/releases/<distribution>/releasenotes` (for
example https://www.debian.org/releases/stable/releasenotes )

An automatic task updates the www-master GIT repository from salsa and
builds the Release Notes for different releases.

Build logs are available at
   https://www-master.debian.org/build-logs/webwml/release-notes.log


The automatic task that builds the Release Notes is:
   https://salsa.debian.org/webmaster-team/cron/blob/master/parts/7release-notes
which is called by the `often` task on `www-master`:

```
# m h               dom mon dow command
 24 3,7,11,15,19,23 *   *   *   /srv/www.debian.org/cron/often
```

CI Builds
---------

The release-notes are built after each push to salsa.debian.org via a
gitlab pipeline.
